<?php
if (!defined('IN_FINECMS')) exit('No permission resources');

return array(
	'key' => 10,
    'name'        => '病毒扫描',
    'author'      => 'cctv',
    'version'     => '1.0',
    'typeid'      => 1,
    'description' => '检测网站是否存在非法木马攻击等恶意代码',
    'fields'      => array()
);